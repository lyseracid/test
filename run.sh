#!/bin/bash

STAGE="[${1:-build}]"
INCREMENT=${2:-25}

echo $STAGE Start
uname -a

for i in $(seq 0 $INCREMENT 100); do
    echo Progress... ${i}%
    sleep 1
done

echo $STAGE Success

